import { Component } from '@angular/core';

@Component({
  selector: 'power-booster',
  template: `
    <h3>Power Booster</h3>
    <p>Super power boost: {{2 | exponentialStrength: 10}}</p>
  `
})
export class PowerBoosterComponent { }