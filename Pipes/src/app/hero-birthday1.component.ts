import { Component } from '@angular/core';

@Component ({
    selector: 'hero-birthday',
    template: `

    <p>The hero's birthday is  {{ birthday | date | uppercase }}</p>

    `,

})

export  class HeroBirthdayComponent {
    birthday = new Date(1988, 3, 15);
}